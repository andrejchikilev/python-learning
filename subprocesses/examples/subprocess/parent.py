#!/usr/bin/env python3
import os
import subprocess
import sys

child = os.path.join(os.path.dirname(__file__), "./child.py")
word  = 'word'
file = [
    './parent.py',
    './child.py',
]

if __name__ == "__main__":
    pipes = []
    for i in range(0,2):
        command = [sys.executable, child]
        pipe = subprocess.Popen(command, stdin=subprocess.PIPE)
        pipes.append(pipe)
        pipe.stdin.write(b''.join((word.encode("utf8"), b"\n")))
        pipe.stdin.write(b''.join((file[i].encode("utf8"), b"\n")))
        pipe.stdin.close()

    while pipes:
        pipe = pipes.pop()
        pipe.wait()