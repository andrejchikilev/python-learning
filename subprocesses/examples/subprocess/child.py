#!/usr/bin/env python3
import sys
import os

word = sys.stdin.readline().rstrip().encode("utf8")
filename = sys.stdin.readline().rstrip().encode("utf8")
print(f'Subprocess: #{os.getpid()}')

try:
    with open(filename, "rb") as fh:
        line_number = 0
        while True:
            current = fh.readline()
            line_number += 1
            if not current:
                break
            if (word in current ):
                print(f"    find: {filename} {word} at line {line_number}")
except :
    raise Exception