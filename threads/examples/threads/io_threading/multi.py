#!/usr/bin/env python3
import time
import requests
import threading

pairs = ['BTC_LTC', 'BTC_ETH', 'BTC_USD', 'BTC_EUR', 'BTC_PLN', 'BCH_BTC', 'EOS_BTC', 'EOS_USD', 'BCH_RUB', 'BCH_ETH']

def get_rates(pair):
    local_start_time = time.time()
    try:
        requests.get("https://api.exmo.com/v1/order_book/?pair={pair}&limit=1000".format(pair=pair))
    except Exception as e:
        print(e)
    print("Пара {pair}, время работы функции: {t:0.4f}".format(pair=pair, t=time.time()-local_start_time))

global_start_time = time.time()

threads = []
for pair in pairs:
    # Подготавливаем потоки, складываем их в массив
    threads.append(threading.Thread(target=get_rates, args=(pair,)))

# Запускаем каждый поток
for thread in threads:
    thread.start()

# Ждем завершения каждого потока
for thread in threads:
    thread.join()

print('Общее время работы {s:0.4f}'.format(s=time.time()-global_start_time))