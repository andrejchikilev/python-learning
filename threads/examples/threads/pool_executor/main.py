from concurrent.futures import ThreadPoolExecutor
import threading

def task(n):
    print(f'    Processing {n}. Thread #{threading.get_ident()}')

def main():
    print("Starting ThreadPoolExecutor №1")
    with ThreadPoolExecutor(max_workers=3) as executor:
        future = executor.submit(task, (2))
        future = executor.submit(task, (3))
        future = executor.submit(task, (4))
    print("Starting ThreadPoolExecutor №2")
    with ThreadPoolExecutor(max_workers=4) as executor:
        future = executor.map(task, range(6))
    print("All tasks complete")

if __name__ == '__main__':
    main()