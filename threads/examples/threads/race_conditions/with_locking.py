#!/usr/bin/env python3
import logging
import time
import threading
from concurrent.futures import ThreadPoolExecutor


class FakeDatabase:
    def __init__(self):
        self.value = 0
        self._lock = threading.Lock()
    def update(self, name):
        logger.info(f'Thread {name}: starting update')
        logger.debug(f'Thread {name} about to lock')
        with self._lock:
            logger.debug(f'Thread {name} has lock')
            local_copy = self.value
            local_copy += 1
            time.sleep(0.1)
            self.value = local_copy
            logger.debug(f'Thread {name} about to release lock')
        logger.debug(f'Thread {name} after release')
        logger.info(f'Thread {name}: finishing update')


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(
        format=format,
        level=logging.DEBUG,
        datefmt="%H:%M:%S"
    )
    logger = logging.getLogger(__name__)
    database = FakeDatabase()
    logger.info(f'Testing update. Starting value is {database.value}.')
    with ThreadPoolExecutor(max_workers=2) as executor:
        for index in range(2):
            executor.submit(database.update, index)
    logger.info(f'Testing update. Ending value is {database.value}.')