import json
import argparse

parser = argparse.ArgumentParser(description='Serialize python primitive types to json')
parser.add_argument('-f', '--file', default='dump.json', help='open json file')

def main():
    args = parser.parse_args()
    # read and deserialize from file
    with open(args.file, 'r') as json_data:
        data = json.load(json_data)
        print(data)

    # deserialize from string
    with open(args.file, 'r') as f:
        json_string = f.read()
        data = json.loads(json_string)
        print(data)

if __name__ == "__main__":
    main()