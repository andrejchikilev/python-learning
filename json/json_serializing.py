import json
import argparse

DATA = {
    "firstName": "Jane",
    "lastName": "Doe",
    "hobbies": ["running", "sky diving", "singing"],
    "age": 35,
    "children": [
        {
            "firstName": "Alice",
            "age": 6,
        },
        {
            "firstName": "Bob",
            "age": 8,
        }
    ],
    "is_married": True,
}

parser = argparse.ArgumentParser(description='Serialize python primitive types to json')
parser.add_argument('-f', '--file', action='store_true', help='Write to file')
parser.add_argument('-s', '--string', action='store_true', help='Write to string')
parser.add_argument('-i', '--indent', action='store_true', help='Add readable identation')


def main():
    args = parser.parse_args()
    if args.indent:
        # Add beauty indentation
        json_data = json.dumps(DATA, indent=4)
    else:
        json_data = json.dumps(DATA)
    # write to file
    if args.file:
        with open('dump.json', 'w') as f:
            f.write(json_data)
    # write to console
    if args.string:
        print(json_data)

if __name__ == "__main__":
    main()