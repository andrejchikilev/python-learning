#!/usr/bin/env python3

# Нужно распарсить GET-запрос в словарь:
# https://duckduckgo.com/?q=get+request+example&t=ffab&df=y&ia=web

# протокол://адрес?парам1=знач1&парам2=знач2...

URL = 'https://www.google.com/search?q=get+query&client=firefox-b-d&tbas=0&tbs=li:1&sxsrf=ACYBGNTq07MTbfIzTO1wRQUzBA-5b5XxBg:1571296420516&source=lnt&sa=X&ved=0ahUKEwjk7uyn36LlAhVu2aYKHV8pASkQpwUIJw&biw=1366&bih=648'

# Словарь должен быть вида:
# {
#     'protocol': 'https',
#     'address': 'duckduckgo.com/'
#     'params': {
#         'q': 'get request example',
#         't': 'ffab',
#         'df': 'y',
#         'ia': 'web',
#     }
# }

# 1. С помощью методов строк

# 2. C помощью регулярных выражений