#!/usr/bin/env python3


API_RESPONSE_EXAMPLE = {
    "coord": {
        "lon": -0.13,
        "lat": 51.51
    },
    "weather": [
        {
            "id": 300,
            "main": "Drizzle",
            "description": "light intensity drizzle",
            "icon": "09d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 280.32,
        "pressure": 1012,
        "humidity": 81,
        "temp_min": 279.15,
        "temp_max": 281.15
    },
    "visibility": 10000,
    "wind": {
        "speed": 4.1,
        "deg": 80
    },
    "clouds": {
        "all": 90
    },
    "dt": 1485789600,
    "sys": {
        "type": 1,
        "id": 5091,
        "message": 0.0103,
        "country": "GB",
        "sunrise": 1485762037,
        "sunset": 1485794875
    },
    "id": 2643743,
    "name": "London",
    "cod": 200
}

# 1. Выведите текст на основе данных из словаря:
# "В <название_города> <градусов_цельсия> ºC, влажность <влажность> %"

# 2. Напишите функцию, которая будет будет создавать из введенной через пробел строки список

# 3. Модифицируйте функцию 2 так, чтобы при вводе целого числа, оно преобразовывалось в число