#!/usr/bin/env python3


# 1.
# Написать функцию encode(in_filename, out_filename)
# in_filename - входной файл
# out_filename - выходной файл
# Функция должна заменять все последовательности повторяющихся символов из
# in_filename на <число_повторов><символ>. Склеивать в одну строку и записывать в 
# out_filename

# Пример содержимого для out_filename:

# 5x4f8d5i10e4y10w6v2j3l


# 2.
# Написать функцию decode(in_filename, out_filename)
# Функция должна привести результат действия функции encode к исходной строке
# 