#!/usr/bin/env python3

# 1. Создать модуль custom_io 
# 2. Написать функцию, которая принимает на вход строку, переобразует в список (по пробелам) и возвращает его.
# 3. Написать функцию format_print, которая принимает число, список или строку и возвращает отформатированную строку вида:
#    'Ответ: <число>'
#    'Ответ: [<элемент>, <элемент>, <элемент>, ...]"
#    'Ответ: "<Строка>"
#    в зависимости от типа аргумента
# 4. Создать модуль file_io
# 5. В нем написать функцию write_string, которая принимает в качестве аргументов строку, имя файла и ключ "write"(по умолчанию) или "append" и в зависимости от ключа записывает или добавляет в конец файла полученную строку
# 6. Написать функцию read_string, которая принимает имя файла, читает оттуда все строки и возвращает список строк.
# 7. Собрать оба модуля в пакет read_write
# 8. Решить какую либо задачу из предыдущих, используя собственный пакет